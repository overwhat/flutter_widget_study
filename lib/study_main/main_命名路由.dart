import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Material App',
        routes: {
          '/home': (context) => Home(),
          '/a': (context) => A(),
          '/b': (context) => B(),
        },
        initialRoute: '/b', //指定应用启动时显示的初始路由
        home: Home());
  }
}

// 首页
class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Home')),
      body: Center(
        child: ElevatedButton.icon(
            onPressed: () {
              // 跳转到A页面
              Navigator.pushNamed(context, '/a');
            },
            icon: const Icon(Icons.home),
            label: const Text('Home')),
      ),
    );
  }
}

// A页面
class A extends StatefulWidget {
  const A({super.key});

  @override
  State<A> createState() => _AState();
}

class _AState extends State<A> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('A')),
      body: Center(
        child: ElevatedButton.icon(
            onPressed: () {
              // 跳转到B页面
              Navigator.pushNamed(context, '/b');
            },
            icon: const Icon(Icons.home),
            label: const Text('A')),
      ),
    );
  }
}

// B页面
class B extends StatefulWidget {
  const B({super.key});

  @override
  State<B> createState() => _BState();
}

class _BState extends State<B> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('B')),
      body: Center(
        child: ElevatedButton.icon(
            onPressed: () {
              //返回上一个页面
              Navigator.pop(context);
            },
            icon: const Icon(Icons.home),
            label: const Text('B')),
      ),
    );
  }
}
